// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('convatarApp', {
  state: () => ({
    showMenuModel : {},
    alertModel: {
      isAlert: false,
      alertType: '',
      alertText: ''
    }
  }),
  getters: {},
  actions: {},
})