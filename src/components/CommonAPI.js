// local vue api axios instance
import axios from 'axios';
import { useRouter } from 'vue-router';
import { useAuthStore } from "@/store/auth";

const router = useRouter();
const authStore = useAuthStore();
const baseUrl = import.meta.env.VITE_APP_API_BASE_URL;

// CAFE24 OAUTH HTTP GET CALL
function httpGetAxiosToOAuth(endpoints, param) {
    const resourceURL = endpoints;
    return axios({
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        url: resourceURL,
        method: 'get'
    })
}

//HTTP GET CALL
function httpGetAxios(endpoints, param) {
    const resourceURL = baseUrl + endpoints;
    return axios({
        headers: {
            // 'Access-Control-Allow-Origin': '*',
        },
        url: resourceURL,
        method: 'get'
    })
}

//HTTP POST CALL
function httpPostAxios(endpoints, param) {
    const resourceURL = baseUrl + endpoints;
    return axios.post(resourceURL, param, {
        headers: {
            // 'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json; charset=utf-8',
        }
    })
}

//HTTP POST CALL WITH TOKEN
function httpPostAxiosWithJWT(endpoints, param) {
    const resourceURL = baseUrl + endpoints;
    return axios.post(resourceURL, param, {
        headers: {
            // 'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Bearer ' + authStore.token,
            'target': 'GESTURE'
        }
    })
}

function apiInstance() {
    const instance = axios.create({
        baseURL: baseUrl,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
    });
    return instance;
}

const signOut = () => {
    authStore.$reset();
    location.href = "/signin";
}

export { httpGetAxiosToOAuth, httpPostAxios, httpPostAxiosWithJWT, httpGetAxios, apiInstance, signOut };