/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'

// Global Style
import '@/styles/settings.scss'

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'

const app = createApp(App);

registerPlugins(app);

app.mount('#app');

let ip = '';
let countryCd = 'KR';
// const ipObj = abcd;
// if(ipObj.ip) ip = ipObj.ip;
// if(ipObj.country.code) countryCd = ipObj.country.code;

app.provide('clientIp', ip);
app.provide('countryCd', countryCd);