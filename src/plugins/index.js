/**
 * plugins/index.js
 *
 * Automatically included in `./src/main.js`
 */

// Plugins
import { loadFonts } from './webfontloader'
import vuetify from './vuetify'
import router from '../router'
import pinia from '../store'
import axios from 'axios';

export function registerPlugins (app) {
  loadFonts();
  app.config.globalProperties.$axios = axios;
  app.config.globalProperties.countryCd = 'KR';
  app.config.globalProperties.toRoute = (link) => {
    const to = { name: link };
    router.push(to);
  };
  app.config.globalProperties.keepDevinggg = () => {
    alert('만드는 중...');
  };

  app
  .use(vuetify)
  .use(router)
  .use(pinia)
  
}
