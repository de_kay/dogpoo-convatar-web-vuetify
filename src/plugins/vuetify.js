// Styles
import '@mdi/font/css/materialdesignicons.css'

// Global Style
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'
import colors from 'vuetify/lib/util/colors'
import DateFnsAdapter from '@date-io/date-fns'
import enUS from 'date-fns/locale/en-US'
import ko from 'date-fns/locale/ko'


// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  date: {
    adapter: DateFnsAdapter,
    locale: {
      en: enUS,
      ko: ko,
    },
  },
  theme: {
    defaultTheme: 'darky',
    themes: {
      darky: {
        dark: true,
        colors: {
          primary: '#2A2A30',
          secondary: '#15181d',
          background: '#212126',
          subBackground: '#2A2A2A',
        },
      },
      lighty: {
        light: true,
        colors: {
          primary: '#FAFAFA',
          secondary: '#F5F5F5',
          background: '#ECEFF1',
        }
      }
    }
  },
  methods: {
    getDisplayForm() {
      console.log(this.$vuetify.display.xs)
      console.log(this.$vuetify.display.sm)
      console.log(this.$vuetify.display.md)
      console.log(this.$vuetify.display.lg)
      console.log(this.$vuetify.display.xl)
      console.log(this.$vuetify.display.xxl)
    }
  },
  // components: {
  //   VSkeletonLoader
  // }
})