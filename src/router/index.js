import { createRouter, createWebHistory } from 'vue-router'
import { useAuthStore } from "@/store/auth";

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/Default.vue'),
    children: [
      {
        path: '/',
        name: 'Home',
        component: () => import('@/components/home/MainView.vue'),
        meta: { roles: ['NORMAL'] },
      },

      // 데이터
      {
        path: '/data/timeFormatter',
        name: 'TimeFormatter',
        component: () => import('@/components/views/data/TimeFormatter.vue'),
        meta: { roles: ['NORMAL'] },
      },
      {
        path: '/data/incodeConverter',
        name: 'IncodeConverter',
        component: () => import('@/components/views/data/IncodeConverter.vue'),
        meta: { roles: ['NORMAL'] },
      },

      // 단위
      {
        path: '/unit/byteConverter',
        name: 'ByteConverter',
        component: () => import('@/components/views/unit/ByteConverter.vue'),
        meta: { roles: ['NORMAL'] },
      },
      {
        path: '/unit/distanceConverter',
        name: 'DistanceConverter',
        component: () => import('@/components/views/unit/DistanceConverter.vue'),
        meta: { roles: ['NORMAL'] },
      },
      {
        path: '/unit/areaConverter',
        name: 'AreaConverter',
        component: () => import('@/components/views/unit/AreaConverter.vue'),
        meta: { roles: ['NORMAL'] },
      },

      // 숫자
      {
        path: '/numeric/baseConverter',
        name: 'BaseConverter',
        component: () => import('@/components/views/number/BaseConverter.vue'),
        meta: { roles: ['NORMAL'] },
      },

      // 금융
      {
        path: '/fund/currencyConverter',
        name: 'CurrencyConverter',
        component: () => import('@/components/views/fund/CurrencyConverter.vue'),
        meta: { roles: ['NORMAL'] },
      },
    ]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior() {
    window.scrollTo(0,0);
  }
})

router.beforeEach((to, from, next) => { // 페이지를 이동하기 전에 호출되는 함수
  const authStore = useAuthStore();
  if ((to.name === "FindPassword") && !authStore.isLoggedIn) {
    // login으로 가고 있지 않고 로그인되어 있지 않으면 /login으로 redirect
    // 로그인하지 않은 상태에서 /를 요청하는 경우 (프로젝트가 처음 실행될 때)
    router.replace({
      name: 'SignIn',
      state: {
        target : to.name
      }
    })
  } else if ((to.name === "SignIn" || to.name === "SignUp" || to.name === "FindPassword") && authStore.isLoggedIn) {
    // login으로 가고 있고 로그인되어 있으면 /으로 redirect
    // 로그인한 상태에서 /login을 요청하는 경우
    next("/");
  } else {
    next();
  }
});

export default router
